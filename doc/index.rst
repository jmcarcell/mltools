.. mltools documentation master file, created by
   sphinx-quickstart on Tue Sep 25 13:10:56 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to mltools's documentation!
===================================

.. toctree::
   :maxdepth: 2

   modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
