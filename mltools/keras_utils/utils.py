# coding: future_fstrings
from keras import backend as K
from keras.utils import multi_gpu_model

def rmse(y_true, y_pred):
    return K.sqrt(K.mean(K.square(y_pred - y_true)))

number_of_gpu_available = None
def gpus_available():
    global number_of_gpu_available
    if number_of_gpu_available is None:
        number_of_gpu_available = len(K.tensorflow_backend._get_available_gpus())
    return number_of_gpu_available

def make_parallel_model(model):
    print(f'Building model with {gpus_available()} gpus available...')
    try:
        parallel_model = multi_gpu_model(model, gpus=gpus_available())
    except ValueError:
        parallel_model = model
    return parallel_model
