# coding: future_fstrings
import numpy as np
import pandas as pd

def train_test_split_flat_distribution(df, samples_per_bin, var, bins,
                                       event_var=None,
                                       test_samples_per_bin=None,
                                       test_samples=None, verbose=False,
                                       behaviour_train='include',
                                       behaviour_test='train',
):
    """
    Split a dataset into a test and training set with at least n samples in
    each bin

    There will be at least n samples in each bin of the variable var given
    by bins. Otherwise a message is printed showing the number of samples that
    will go into that bin

    :param df: DataFrame
    :param n: int  
    :param var:
    :param bins:

    :returns: tuple of DataFrames (train, test)

    >>> train, test = train_test_split_flat_distribution(df, 1000, 'energy_mc')
    """
    train = [] 
    test = []

    if behaviour_test == 'train':
        behaviour_test = behaviour_train
    dic = {'include': 0, 'exclude_left': 1, 'exclude': 2, 'exclude_right': 3}
    behaviour_train = dic[behaviour_train]
    behaviour_test = dic[behaviour_test]

    # Coding 
    # 0 include
    # 1 exclude_left
    # 2 exclude
    # 3 exclude_right
    # > 0 and <= 2 to exclude left
    # > 0 and >= 2 to exclude right

    for i, (bin_number, group) in enumerate(df.groupby(np.digitize(df[var], bins=bins))):
        if bin_number == 0 and (behaviour_train > 0 and behaviour_train <= 2):
            if not (behaviour_test > 0 and behaviour_test <= 2):
                test.append(group)
            continue
        if bin_number == len(bins) and (behaviour_train > 0 and behaviour_train >= 2):
            if not (behaviour_test > 0 and behaviour_test >= 2):
                test.append(group)
            continue
            

        if len(group) < samples_per_bin: # If size of the group < n, all go to training
            if verbose:
                print("Not enough samples in bin: ", end='')
                if bin_number == 0:
                    print(f"[-inf, {bins[0]}]", end='')
                elif bin_number == len(bins):
                    print(f"[{bins[-1]}, inf]", end='')
                else:
                    print(f"[{bins[bin_number-1]}, {bins[bin_number]}]", end='')
                print(f", {len(group)}/{samples_per_bin} requested")
            train.append(group)
            continue

        if event_var is None:
            group = group.sample(frac=1)
            train.append(group.iloc[:samples_per_bin])
            test.append(group.iloc[samples_per_bin:test_samples_per_bin])
        else:
            groups = [df for _, df in group.groupby(event_var, sort=False)]
            print("groups done")
            np.random.shuffle(groups)
            shuffled_group = pd.concat(groups)
            j = samples_per_bin
            pos = shuffled_group.columns.get_loc(event_var)
            while shuffled_group.iat[j, pos] == shuffled_group.iat[samples_per_bin-1, pos]:
                j += 1
            train.append(shuffled_group.iloc[:j])
            test.append(shuffled_group.iloc[j:])

    train = pd.concat(train) if train else pd.DataFrame()
    if test_samples is not None:
        test = pd.concat(test).sample(frac=1).iloc[:test_samples] if test else pd.DataFrame()
    else:
        test = pd.concat(test) if test else pd.DataFrame()

    print(f"Length of training = {len(train)} and test = {len(test)}")
    return train, test
