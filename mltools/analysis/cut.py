# coding: future_fstrings
from .defs import SEP
import numpy as np
import pandas as pd

def cut(dataset, cuts_ls, statistics=False, energy_bins=None):
    new_dataset = [[]] * len(dataset)
    if statistics:
        events = [pd.DataFrame(columns=['events_before', 'events_after'],
                                    index=[f'$10^{{{x:.1f}}}<E<10^{{{y:.1f}}}$'
                                                for x, y in zip(energy_bins[:-2],
                                                                energy_bins[1:-1])] +
                                        [f'$E>10^{{{energy_bins[-2]:.1f}}}$'])
                  for i in range(len(dataset))]
        if statistics == True:
            statistics = 0
    else:
        statistics = -1
    for i, df in enumerate(dataset):
        print(f"Applying cuts to dataset {i}")
        m = np.full(len(df), True)
        for j, cut in enumerate(cuts_ls):
            print(np.count_nonzero(m))
            m &= cut(df)
            if j == statistics:
                ndf = df[m]
                tmp = ndf.groupby('en').head(1)
                events[i]['events_before'] = tmp.groupby(np.digitize(tmp['en'], bins=energy_bins)).size().values

        df = df[m]
        values, counts = np.unique(df['en'], return_counts=True)
        df = df[df['en'].isin(values[counts >= 4])]
        df = df.groupby('en', as_index=False).nth([0, 1, 2, 3])
        if statistics:
            tmp = df.groupby('en').head(1)
            events[i]['events_after'] = tmp.groupby(np.digitize(tmp['en'], bins=energy_bins)).size().values
        new_dataset[i] = df
    print(SEP)
    if statistics:
        return new_dataset, events 
