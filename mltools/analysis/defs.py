# coding: future_fstrings
SEP = "\n##############################\n"

########## RISETIME and ToD ########## 

VEM = r'$S\ [\rm VEM]$'
RT = r'$t_{1/2}\ [\rm ns]$'
FT = r'$t_{50-90}\ [\rm ns]$'
RTR = r'$\frac{t_{1/2}}{r}\ [\mathrm{ns}\cdot\mathrm{m}^{-1}]$'
ZEN = r'$\sec\,\theta$'
STH = ZEN
ZEN_MC = r'$\sec\,\theta^{(\mathrm{MC})}$'
E = r'$\log_{10}\,(E/\mathrm{eV})$'
E_MC = r'$\log_{10}\,(E^{(\mathrm{MC})}/\mathrm{eV})$'
XI = r'$\xi\ [\mathrm{ns}\cdot\mathrm{m}^{-1}]$'
TOD = r'$\overline{\rm ToD}\ [\rm ns\cdot m^{-1}]$'
DTOD = r'$\Delta\overline{\rm ToD}$ [$\rm ns\cdot m^{-1}$]'
TOD_UNITS = r'[$\rm ns\cdot m^{-1}$]'
RDIFF = r'$|r_1-r_2|\ [\rm m]$'
RMEAN = r'$(r_1+r_2)/2\ [\rm m]$'
VEMDIFF = r'$|S_1-S_2|\ [\rm VEM]$'
VEMMEAN = r'$(S_1+S_2)/2\ [\rm VEM]$'
SIG1 = r'$\langle x^g\rangle-\langle x\rangle\ [\rm ns]$'
SIG2 = r'$x^g_j-\langle x^g\rangle\ [\rm ns]$'
R = r'$r\ [\rm m]$'
AZIMUTH = r'$\zeta\ [\rm radians]$'

########## MUON ########## 

MUON_DIFF = '$S_\mu^\mathrm{true}-S_\mu^\mathrm{pred}$ [VEM]'
MUON_TRUE = '$S_\mu^\mathrm{true}$ [VEM]'
MUON_PRED = '$S_\mu^\mathrm{pred}$ [VEM]'
MUON_RELP =\
'$(S_\mu^\mathrm{true}-S_\mu^\mathrm{pred})/S_\mu^\mathrm{true}\cdot100$ [%]'
MUON_REL =\
'$(S_\mu^\mathrm{true}-S_\mu^\mathrm{pred})/S_\mu^\mathrm{true}$ '
MUON_STD = r'$\sigma(S_\mu^\mathrm{true}-S_\mu^\mathrm{pred})$ [VEM]'

########## XMAX ########## 

XMAX = r'$X_\max\ [\rm g/\cm^2]$'

XMAX_TRUE = r'$X_\max^{\rm true}\ [\rm g/cm^2]$'
XMAX_PRED = r'$X_\max^{\rm pred}\ [\rm g/cm^2]$'
XMAX_DIFF = r'$X_\max^{\rm pred}-X_\max^{\rm true}\ [\rm g/cm^2]$'

########## Plots ########## 

SIZE_X = {1: 4.921259842519685, 2: 6.299212598425196, 'many': 6.299212598425196,}
SIZE_Y = {1: 3.149606299212598, 2: 4.724409448818897, 'many': 7.874015748031496}
