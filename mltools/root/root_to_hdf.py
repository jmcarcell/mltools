import uproot
import pandas as pd
import numpy as np
import os

def root_to_hdf(filepath, filename=None, output_folder=None,
                return_df=False, tree_name='Tree', dtype=None):
    """
    Transform a ROOT tree given by filepath into a pandas DataFrame
    Either a filename is given for the output or an output folder where
    the files will be named the same but with '.root' changed to '.hdf'

    :param filepath: string
    :param filename: string
    :param output_folder: string
    :param return_df: bool
    :param tree_name: name of the tree of the ROOT file
    :param dtype: dtype used for the output pandas DataFrame (by default)

    :returns: DataFrame if return_df is True (and cache is not True) otherwise None
    """

    if filename is None and output_folder is None:
        raise ValueError("A filename or output folder has to be given for the output")

    ind_root = filepath.find('.root')
    ind_slash = filepath.rfind('/')
    base = filepath[ind_slash + 1: ind_root]
    output_name = base + '.hdf'
    if output_folder is not None:
        filename = output_folder + ('/' if output_folder[-1] != '/' else '') +\
            output_name

    upr = uproot.open(filepath)
    upr = upr[tree_name].arrays(upr[tree_name].keys())

    data = pd.DataFrame()
    dic = {}
    ary = []
    for key, val in upr.items():
        if isinstance(val, np.ndarray):
            dic[key.decode('UTF-8')] = val
        else:
            shape = val.shape[0]
            ary.append(val.flatten().reshape((shape, -1)))
    if dic:
        data = pd.DataFrame(dic)
    elif ary:
        num = len(ary)
        entries = len(ary[0])
        if num != 1:
            index = pd.MultiIndex.from_arrays([np.repeat(np.arange(num), entries), np.tile(np.arange(entries), num)])
        else:
            index = None
        data = pd.DataFrame(np.concatenate(ary), index=index, dtype=dtype)

    data.to_hdf(filename, 'data', format='f', mode='w')
    if return_df:
        return data

if __name__ == '__main__':
    import argparse
    from sys import argv

    parser = argparse.ArgumentParser(description='Transform ROOT files to HDF files')
    parser.add_argument('files', type=str, nargs='*',
                         help='ROOT files that will be converted to hdf')
    parser.add_argument('--dtype', type=np.dtype,
                        help="dtype used for pandas DataFrame. Example: 'float16',  'int64'")

    args = parser.parse_args()
    if args.files is None:
        parser.error('files not specified')
    if args.dtype is not None:
        args.dtype = np.dtype(args.dtype)
    output_folder = os.getcwd()
    for filepath in args.files:
        root_to_hdf(filepath, output_folder=output_folder, dtype=args.dtype)
