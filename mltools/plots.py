# coding: future_fstrings
import matplotlib.pyplot as plt
import matplotlib as mpl
from .profile import profile
import numpy as np

class Figures:
    def __init__(self):
        # Dictionary of figures
        self.figures = {}
        self.figsize = {1: (4.921259842519685, 3.149606299212598),
                    2: (6.299212598425196, 4),
                    4: (6.299212598425196, 4.724409448818897)}
        # self.figsize = {1: (6.299212598425196, 4.724409448818897),
        #             2: (6.299212598425196, 4.724409448818897),
        #             4: (6.299212598425196, 4.724409448818897)}

    def get_figure(self, number_of_axes, **kwargs):
        try:
            return self.figures[number_of_axes]
        except KeyError:
            fig, axls = plt.subplots(*{1: [1, 1], 2: [2, 1], 4: [2, 2]}[number_of_axes],
                                        figsize=self.figsize[number_of_axes], squeeze=False)
            axls = axls.flatten()
            self.figures[number_of_axes] = (fig, axls)
            return fig, axls

    def delete_figure(self, fig=None, number_of_axes=None):
        if number_of_axes:
            del self.figures[number_of_axes]
        elif fig:
            for key, val in self.figures.items():
                if val == fig:
                    del self.figres[key]
                    break
        else:
            raise TypeError

class Plots:
    def __init__(self, dataset, presentation_path=None, savefig=False):

        # Initialize self.dataset
        self.set_dataset(dataset)

        self.names = None
        self.params = {}

        # Counter for figure names
        self.counter = 1
        self.savefig = savefig

        # Figures object with information about the currently opened
        # figures
        self.figures = Figures()

        if presentation_path is not None:
            from .presentation import Presentation
            self.presentation = Presentation(presentation_path)
        else:
            self.presentation = None

    def get_figure(self, *args, **kwargs):
        return self.figures.get_figure(*args, **kwargs)

    def add_figure_to_presentation(self, fig, savefig=False):
        """
        Add a figure to the current presentation
         
        :param fig: matplotlib figure
        
        :returns: nothing
        
        :raise: TypeError if there is not an available presentation
        """
        try:
            self.presentation.add_fig(fig)
            if savefig:
                fig.savefig(f'{self.counter}')
                self.counter += 1
        except AttributeError:
            raise TypeError('There is no presentation to add a figure to')

    def set_names(self, dic):
        self.names = dic

    def set_dataset(self, dataset):
        """
        Change the current dataset to `dataset`
        
        :param dataset: pandas DataFrame or list of pandas DataFrame
        
        :returns: nothing
        """
        if isinstance(dataset, list):
            self.dataset = dataset
        else:
            self.dataset = [dataset]

    def set_defaults(self, names=None, params=None):
        self.names = names
        self.params = params

    def plot(self, plot_type, **kwargs):

        fun = {'histogram': histogram, 'profile': prof_plot, 'scatter':
               scatter}[plot_type]

        fig, axls = self.pick_figure(**kwargs)
        for ax in axls:
            ax.clear()

        # Logic for dealing with parameters and default parameters
        # Must be redone
        if 'x' in kwargs:
            var = kwargs['x']
            if var in self.params:
                dic = self.params[var]
                for key in (key for key in dic if key not in kwargs):
                    kwargs[key] = dic[key]
        

        # If xlabel and ylabel are not provided
        # use the names of the columns or 'Entries' for histograms
        # if 'xlabel' not in kwargs:
        #     kwargs['xlabel'] = kwargs['xvar']
        # if 'ylabel' not in kwargs and 'yvar' in kwargs:
        #     kwargs['ylabel'] = kwargs['yvar']
        # elif 'ylabel' not in kwargs:
        #     kwargs['ylabel'] = 'Entries'
                        
        if kwargs.get('use_names'):
            kwargs['names'] = self.names

        # The state of include_relative and include_std are saved
        # so that it is not set for the first call to the plot functions
        # and the legend can be put only on the first plot
        include_relative = kwargs.get('include_relative', None)
        kwargs['include_relative'] = False
        include_std = kwargs.get('include_std', None)
        kwargs['include_std'] = False

        _text(fig, axls, self.dataset, **kwargs)
        fun(dataset=self.dataset, axls=axls, **kwargs)

        kwargs['include_relative'] = include_relative
        kwargs['include_std'] = include_std
        if kwargs.get('include_relative'):
            kwargs['y'] = 'rel'
            _text(fig, axls[len(axls)//2 :], self.dataset, **kwargs)
            fun(dataset=self.dataset, axls=axls[len(axls)//2 :], **kwargs)
        elif kwargs.get('include_std'):
            # kwargs['yvar'] = 'std' 
            kwargs['prof_kws']['use'] = 'std'
            _text(fig, axls[len(self.dataset) :], self.dataset, **kwargs)
            fun(dataset=self.dataset, axls=axls[len(self.dataset) :], **kwargs)
        self.presentation.add_fig(fig)

        if self.savefig:
            fig.savefig(f'{self.counter}')
            self.counter += 1

        # if kwargs.get('savefig', None):
        #     fig.savefig(kwargs['figname'])

        # fig.suptitle is permanently on the figure
        # even when the axes object is cleared
        fig.suptitle(t='')

    def pick_figure(self, **kwargs):
        """
        Decide the number of axes needed for the figure.
        It defaults to the length of the current dataset.
        If 'same_figure' is set to true then everything is 
        plotted in the same dataset
        If 'include_relative' is set to true then the relative
        plot is also included, adding 1 to the number of axes.
        """

        # Number of axes that we are going to use

        length = len(self.dataset)
        axes = length
        if kwargs.pop('same_figure', False):
            axes = 1
        else:
            axes = length

        double_axis_number = False
        assert (bool(kwargs.get('include_relative')) + bool(kwargs.get('include_std'))) < 2
        for name in ['include_relative', 'include_std']:
            var = kwargs.get(name)
            if var:
                double_axis_number = True
                axes *= 2
        
        fig, axls = self.figures.get_figure(axes)

        if double_axis_number:
            axls = np.concatenate([np.tile(axls[i], length) for i in
                                    range(axes)], axis=0)
        
        return fig, axls

    def hist(self, **kwargs):
        self.plot(plot_type='histogram', **kwargs)

    def profile(self, **kwargs):
        self.plot(plot_type='profile', **kwargs)

    def scatter(self, **kwargs):
        self.plot(plot_type='scatter', **kwargs)


    def close_presentation(self):
        if self.presentation is None:
            raise ValueError('Tried to close presentation when there is none')
        else:
            self.presentation.close()

    def hist_all(self, include_vars=None, exclude_vars=None, **kwargs):
        self.plot_all('histogram', None, include_vars, exclude_vars, **kwargs)

    def profile_all(self, var, include_vars=None, exclude_vars=None,
                    params_dic=None, **kwargs):
        self.plot_all('profile', var, include_vars, exclude_vars, **kwargs)

    def scatter_all(self, var, include_vars=None, exclude_vars=None, **kwargs):
        self.plot_all('scatter', var, include_vars, exclude_vars, **kwargs) 

    def plot_all(self, plot_type, var, include_vars, exclude_vars, **kwargs):

        if exclude_vars is None:
            exclude_vars = []
        if plot_type in ['profile', 'scatter'] and var not in exclude_vars:
            exclude_vars.append(var)

        if include_vars is None:
            include_vars = self.dataset[0].columns

        for xvar in (xvar for xvar in include_vars if xvar not in exclude_vars):

            if 'x' in kwargs:
                print("THIS IS NEVER EXECUTED, RIGHT?")
                var = kwargs['x']
                if var in self.params:
                    print(var, "found in self.params")
                    print(self.params)
                    dic = self.params[var]
                    for key in dic:
                        if key in kwargs and isinstance(kwargs[key], dict):
                            kwargs[key].update(dic[key])
                        else:
                            kwargs[key] = dic[key]

            # if self.params is not None:
            #     kwargs.update(self.params.get(xvar, []))

            if plot_type in ['profile', 'scatter']:
                self.plot(plot_type=plot_type, x=xvar, y=var, **kwargs)
            elif plot_type == 'histogram':
                self.plot(plot_type=plot_type, x=xvar, **kwargs)

def _parse_kwargs_for_plot(dictionary, index):
    """
    Parse dictionary to take into accounts arguments that are a list
    Usually dictionary is coming from the kwargs of the plotting function

    This function returns a new parsed dictionary
    For every element in the dictionary that is a list, the value of the
    list at index `index` is returned with the same key
    For every key that contains 'kws' (usually a dictionary) and that
    dictionary[key] is a non-empty dictionary, if it contains a
    list, the value of that non-empty dictionary for that key is changed to the element
    given by index index of the list
    """
   
    dic = dict(dictionary) # copy needed so as not to overwrite the original dictionary
    for key, val in dictionary.items():
        if isinstance(val, list):
            dic[key] = val[index]
        if 'kws' in key and dictionary[key] and isinstance(dictionary[key], dict):
            dic[key] = dict(dictionary[key]) # copy needed again
                                             # dictionary keeps a reference to
                                             # the dictionaries inside so if
                                             # dic is changed, it is changed
                                             # for dictionary too
            for nkey, nval in dic[key].items():
                if isinstance(nval, list):
                    dic[key][nkey] = nval[index]
    return dic 

def _make_fig(dataset):
    fig, axls = plt.subplots(squeeze=False)
    axls = axls.flatten()
    return fig, axls


""" Rules for the plotting functions

The signature is the same for all of them
plot(x, y, dataset, fig, axls, **kwargs)

- yvar is provided even when not needed (in a histogram)
for compatibility

- Ff fig and axls are not provided, new figure and axls objects are created and
  returned

-
"""

def _histogram(x, y=None, ax=None, **kwargs):
    ax.hist(x, **kwargs.get('hist_kws', {}))
    ax.grid(zorder=10)

def histogram(x, y=None, dataset=None, axls=None, **kwargs):
    return _plot(x, y, dataset, axls, fun=_histogram, **kwargs)

def _scatter(x, y, ax, **kwargs):

    ax.scatter(x, y, **kwargs['scatter_kws'])
    ax.grid(zorder=10)
    if kwargs.get('correlation_line'):
        vals = [min(x.min(), y.min()), max(x.max(), y.max())]
        ax.plot(vals, vals,  'k')
    if kwargs.get('correlation_coefficient'):
        corr = np.corrcoef(x, y)[1, 0]
        ax.text(0.2, 0.8, r'$\rho={:.2f}$'.format(corr),
                transform=ax.transAxes)
        
def scatter(x, y, dataset, axls, **kwargs):
    if 'scatter_kws' not in kwargs:
        kwargs['scatter_kws'] = {}

    # With 8000 points rasterizing at 300 dpi
    # breaks even (similar weight of the figure)
    # It takes much longer to load an
    # unrasterized pdf with many points
    if 'rasterized' not in kwargs['scatter_kws']:
        if max([len(df) for df in dataset]) >= 8000:
            kwargs['scatter_kws']['rasterized'] = True
        else:
            kwargs['scatter_kws']['rasterized'] = False
    return _plot(x, y, dataset, axls, fun=_scatter, **kwargs)

def _prof_plot(x, y, ax, **kwargs):
    xpr, ypr, zpr = profile(x, y, **kwargs.get('prof_kws', {}))
    ax.errorbar(xpr, ypr, yerr=zpr, fmt='o', **kwargs.get('errorbar_kws', {}))
    ax.grid(zorder=10)

def prof_plot(x, y, dataset=None, axls=None, **kwargs):
    return _plot(x, y, dataset, axls, fun=_prof_plot, **kwargs)


def _plot(x, y=None, dataset=None, axls=None, fun=None, **kwargs):

    iterations = 1
    if dataset is not None and not isinstance(dataset, list):
        dataset = [dataset]
    if dataset is not None:
        iterations = len(dataset)

    out = []
    if axls is None:
        fig, axls = _make_fig(iterations)
        out = [fig, axls]

    for i in range(iterations):
        ax = axls[min(i, len(axls)-1)] #This is the same as if len(axls) > i: ax = axls[i]
                             #else: ax = axls[0]

        if dataset is not None:
            xraw = dataset[i][x]
            yraw = dataset[i][y] if y is not None else None
        else:
            xraw = x
            yraw = y if y is not None else None
        parsed_kwargs = _parse_kwargs_for_plot(kwargs, i)
        fun(xraw, yraw, ax, **parsed_kwargs)

    if parsed_kwargs.get('legend', None):
        if (kwargs.get('include_relative', None) or
            kwargs.get('include_std', None)):
            pass
        else:
            ax.legend(**parsed_kwargs.get('legend_kws', {}))

    
    if (kwargs.get('include_relative', None) or
        kwargs.get('include_std', None)):
        ax.set_xlim(parsed_kwargs.get('rel_xlim', None))
        ax.set_ylim(parsed_kwargs.get('rel_ylim', None))
    else:
        ax.set_xlim(parsed_kwargs.get('xlim', None))
        ax.set_ylim(parsed_kwargs.get('ylim', None))

    if out:
        return out

def _text(fig, axls, dataset, **kwargs):
    if 'title_kws' in kwargs:
        fig.suptitle(**kwargs['title_kws'])
    for i in range(len(dataset)):
        if len(axls) > i:
            ax = axls[i]
        else:
            ax = axls[0]
        if 'xlabel' in kwargs:
            ax.set_xlabel(kwargs['xlabel'])
        if 'ylabel' in kwargs:
            ax.set_ylabel(kwargs['ylabel'])
        if 'names' in kwargs:
            ax.set_xlabel(kwargs['names'].get(kwargs['x'], kwargs['x']))
            try:
                ax.set_ylabel(kwargs['names'].get(kwargs['y'], kwargs['y']))
            except KeyError:
                ax.set_ylabel('Entries')

        if kwargs.get('text_kws', None):
            dic = dict(kwargs['text_kws'])
            if 's' not in dic:
                dic['s'] = dic['text']
            ax.text(transform=ax.transAxes, **dic)

        j = 1
        while kwargs.get(f'text{j}_kws', None) and not\
            (kwargs.get('include_relative', None) or kwargs.get('include_std', None)):
            dic = dict(_parse_kwargs_for_plot(kwargs[f'text{j}_kws'], i))
            if 's' not in dic:
                dic['s'] = dic['text']
            ax.text(transform=ax.transAxes, **dic)
            j += 1
            

        from .plot_utils import add_table
        if kwargs.get('text_info_kws', None):
            ax = axls[min(i, len(axls)-1)]
            var = kwargs['x']
            dic = kwargs['text_info_kws']

            if 'text' not in dic:
                left = ''
                right = ''
                text = ''
                if dic.get('put_entries', False):
                    left += 'Entries' + '\n'
                    right += str(len(dataset[i])) + '\n'
                if dic.get('put_mean', False):
                    left += 'Mean' + '\n'
                    right += f'{dataset[i][var].mean():.2f}' + '\n'
                if dic.get('put_std', False):
                    left += 'Std' + '\n'
                    right += f'{dataset[i][var].std():.2f}' + '\n'
                left = left[:-1]
                right = right[:-1] 
            add_table(left, right, ax, [0.65, 0.65, 0.3, 0.3])
