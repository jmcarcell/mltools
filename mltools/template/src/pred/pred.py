import pandas as pd
import numpy as np
from ..config import options as opt
from .read_model import read_model

def main():
    pipeline = read_model()
