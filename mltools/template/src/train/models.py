from ..config import options as opt
import numpy as np
import pandas as pd

class SelectColumns:
    def _transform(self, data):
        to_drop = []
        to_keep = []
        if to_keep:
            return [pd.DataFrame(df.drop(to_drop, axis=1, errors='ignore')[to_keep]) for df in data]
        return [pd.DataFrame(df.drop(to_drop, axis=1, errors='ignore')) for df in data]
    def transform(self, data):
        return self._transform(data)
    def fit_transform(self, data, y=None):
        return self._transform(data)

class LGBM:
    def __init__(self):
        import lightgbm as lgb
        self.model = lgb.LGBMRegressor(n_estimators=100000, 
                                       objective='rmse', device='gpu',
                                       learning_rate=0.08,
                                       # num_leaves=50,
                                       # min_data_in_leaf=2000,
                                       # max_depth=6,
                                       early_stopping_rounds=1000,
                                       # random_state=1,
        )
        self.model = lgb.LGBMClassifier(n_estimators=100000, 
                                        objective='logloss', device='gpu',
                                        learning_rate=0.08,
                                        # num_leaves=50,
                                        # min_data_in_leaf=2000,
                                        # max_depth=6,
                                        early_stopping_rounds=1000,
                                        # random_state=1,
        )
    def fit(self, data, y=None):
        train, dev = data
        train_y = train.pop('target')
        dev_y = dev.pop('target')
        print('Columns used for training:')
        print(list(train.columns))
        self.model.fit(train, train_y,
                       eval_set=[(train, train_y), (dev, dev_y),],
                       eval_metric='binary_error',
                       verbose=100,
        )
    def predict(self, data):
        return self.model.predict(data[0].drop('target', 1, errors='ignore'))

class XGB:
    def __init__(self):
        import xgboost as xgb
        self.model = xgb.XGBRegressor(n_estimators=100000,
                                      learning_rate=0.05,
                                      #min_split_loss=1e-4, 
                                      tree_method='gpu_hist')
        self.model = xgb.XGBClassifier(n_estimators=100000,
                                      learning_rate=0.05,
                                      #min_split_loss=1e-4, 
                                      tree_method='gpu_hist')
    def fit(self, data, y=None):
        train, dev = data
        train_y = train.pop('target')
        dev_y = dev.pop('target')
        print('Columns used for training:')
        print(list(train.columns))
        self.model.fit(train, train_y,
                       eval_set=[(train, train_y), (dev, dev_y),],
                       early_stopping_rounds=1000,
                       verbose=100,
        )
    def predict(self, data):
        return self.model.predict(data[0])

def model():
    inp = tf.keras.layers.Input(shape=())
    model = tf.keras.models.Model(inputs=inp, outputs=out)
    model.compile(loss=rmse, optimizer='adam')
    model.summary()
    return model

class NeuralNetwork:
    def __init__(self):
        self.model = None

    def build_model(self):
        return model()

    def predict(self, data):
        return self.model.predict(data[0])

    def fit(self, data, y=None):
        batch_size = 256
        train, dev = data
        train = train.sample(n=len(train) // batch_size * batch_size)
        train_y = train.pop('target').values
        dev_y = dev.pop('target').values

        print('Columns used for training:')
        print(list(train.columns))
        
        callbacks = []

        model = self.build_model()
        self.model = model
        model.fit(train_x, train_y, epochs=200, batch_size=batch_size,
                  validation_data=(dev_x, dev_y), verbose=1,
                  callbacks=callbacks)
