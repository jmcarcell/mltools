import pandas as pd
from sklearn.pipeline import make_pipeline
import joblib
from .models import *
from ..config import options as opt

def main():

    steps = {
        0: [SelectColumns, LGBM],
        1: [SelectColumns, XGB],
        2: [SelectColumns, NeuralNetwork]
    }[0]
    for i in range(len(steps)):
        steps[i] = steps[i]()

    # Read the data
    train = pd.read_hdf(opt.processed_directory + opt.train_name)
    dev = pd.read_hdf(opt.processed_directory + opt.dev_name)

    # Define and train the model
    pipeline = make_pipeline(*steps)
    pipeline.fit([train, dev])

    # Save the model
    joblib.dump(pipeline, opt.models_directory + 'pipeline.pkl')
