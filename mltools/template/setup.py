import os
import shutil
import subprocess

text = r"""
from .config import options
from sys import argv
import subprocess

def main():
    def make_train_and_test_dataset():
        from .data import train_and_test
        train_and_test.main()
    def do_train():
        from .train import train
        train.main()
    def do_test():
        from .pred import pred
        pred.main()
    def all_of_the_above():
        make_train_and_test_dataset()
        train()
        test()
    def commit_and_push():
        for command in ['git commit -am "Automatic commit"',
                        'git push',]:
            # Do not use the param capture_output, it is not compatible
            # with python 3.5
            process = subprocess.run([command], stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE, shell=True,
                                     universal_newlines=True,)

            if not process.returncode:
                print(process.stdout)
                print(process.stderr)
            else:
                print("Error when running git commit:", process.stderr)
                break

    def test_commit_and_push():
        do_test()
        commit_and_push()
    def train_commit_and_push():
        do_train()
        commit_and_push()

    print('''
########################################################
#                                                      #
#    {project_name}#
#                                                      #
########################################################''')
    print('Chose what to do (eg: 1 2 3, 1-3)')
    print('''
[1] Make train and test datasets
[2] Train
[3] Train & Commit & Push
[4] Test
[5] Test & Commit & Push
[6] Make datasets, train and test
''')
    dic = {{
        1: make_train_and_test_dataset,
        2: do_train,
        3: train_commit_and_push,
        4: do_test,
        5: test_commit_and_push,
        6: all_of_the_above,
        }}
        
    todo = []
    if len(argv) > 1:
        inp = argv[-1]
    else:
        inp = input()
    if ' ' in inp:
        inp = inp.split(' ')
        todo = [int(elem) for elem in inp]
    else:
        todo.append(int(inp[0]))
        if len(inp) > 1:
            todo.append(int(inp[2]))
    for num in todo:
        dic[num]()
"""

def main():
    if os.path.isfile('src/main.py'):
        print("File src/main.py already exists. Delete file? [y/n]")
        inp = input()
        while (inp.lower() != 'y' and inp.lower() != 'n'):
            print("Incorrect input. Delete file? [y/n]")
            inp = input()
        if inp.lower() == 'n':
            return
        else:
            os.remove('src/main.py')
    print("Short name of the project (the name of the folder): ", end='')
    project_name = input()
    print("Long name of the project (the description shown at startup): ", end='')
    long_name = input()
    with open('src/main.py', 'w') as f:
        f.write(text.format(project_name=long_name))


    if os.path.isdir('.git'):
        shutil.rmtree('.git')
    if os.path.isfile('README.org'):
        os.remove('README.org')
    print("Path of git repo")
    git_repo = input()
    if git_repo:
        subprocess.run(['git', 'init'])
        subprocess.run(['git', 'remote', 'add', git_repo])
    if os.path.isfile('setup.py'):
        os.remove('setup.py')
    
main()
