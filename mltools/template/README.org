* Overview
This is a template for machine learning projects
This repo has the following structure

#+BEGIN_SRC
.
├── config.py         # 
├── data
│  ├── interm        # Intermediate data
│  ├── processed     # Processed data ready for training and prediction
│  └── raw           # Raw data
├── main.py
├── models            # Directory to keep models
├── reports
├── run.py
├── setup.py         
└── src               # Directory with the source code
    ├── config
    │  ├── options.py
    ├── data
    │   └── train_and_test.py
    ├── pred
    │  ├── pred.py
    │  └── read_model.py
    └── train
        ├── models.py
        └── train.py
#+END_SRC

The program is run using 
#+BEGIN_SRC
python3 setup.py
#+END_SRC

* Installation
Clone the repository with 
#+BEGIN_SRC
git clone https://gitlab.com/jmcarcell/mltemplate name-of-the-project --depth 1
cd name-of-the-project
python3 setup.py
#+END_SRC
and follow the instructions
