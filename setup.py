from setuptools import setup

setup(
   name='mltools',
   version='0',
   description='A useful module',
   author='Juan Miguel Carceller',
   packages=['mltools'],  #same as name
   install_requires=['numpy', 'pandas', 'matplotlib'], #external packages as dependencies
)
